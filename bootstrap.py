#!/usr/bin/python3
import sys
from random import choice


def writecsv(filename, data, header):
    file = open(filename, "w")
    file.write(header)
    for line in data:
        file.write(",".join(line) + "\n")
    file.close()


def readcsv(filename):
    file = open(filename, "r")
    content = file.readlines()
    file.close()
    lines = []
    for line in content[1:]:
        lines.append(line.strip("\n").split(","))
    return lines, content[0]


def bootstrap(lines):
    training = []
    test = []
    for i in range(len(lines)):
        training.append(choice(lines))
    for inst in lines:
        if inst not in training:
            test.append(inst)
    return training, test


def main():
    lines, header = readcsv(sys.argv[1])
    train, test = bootstrap(lines)
    print("Amount of instances in testdata:", len(test))
    print("Amount of instances in traindata:", len(train))
    writecsv("train.csv", train, header)
    writecsv("test.csv", test, header)


main()
